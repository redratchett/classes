#ifndef DEMO_H
#define DEMO_H

class Investment 
 {
    private:  //properties
         long double value;
         float interest_rate; 
         


    public:  //methods
          Investment();
          Investment(long double, float);//default constructor 

          void setval(long double);
          long double getval();
	  void setinterest(float);
	  float getinterest();


 };
#endif
