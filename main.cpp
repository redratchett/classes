#include <iostream>
#include "Demo.h"
using namespace std;


int main()
{
Investment inv;
long double temp = 0.0;
long double month_invest;
int year = 0;
long double target = 0.0;
long double initial = 0.0;
float rate = 0.0;

cout <<"How much will you invest at first?" << endl;
cin >> initial;
inv.setval(initial);


cout <<"How much will you pay in monthly payments?" << endl;
cin >> month_invest;
cout <<"What is your interest rate?" << endl;
cin >> rate;
inv.setinterest(rate);

cout<<"What is your target total?"<<endl;
cin >> target;
cout<<target<<endl;

cout<<"Calculating.."<<endl;

temp = inv.getval();

while(inv.getval() < target)
{
temp = (temp + (month_invest * 12)) + (rate * temp);
inv.setval(temp);
year++;
}
cout << "The value at the end of "<<year<<" years would be $"<<inv.getval()<<endl;
cout<<"That is $"<<inv.getval() - target<<" above your target."<<endl;





return 0;

}
